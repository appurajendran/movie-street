<?php

class Poll_model extends CI_Model {

//file name
public function get_nominations()
{
		$sql = "SELECT n.id,cat_id,nomination_name,nomination_subn_ame,img_url,name,status FROM nominations n JOIN category c ON c.id=n.cat_id ORDER BY cat_id";
		$result=$this->db->query($sql);
		//echo $this->db->last_query();exit;
		return $result->result();
}
public function poll_insert($insert_data)
{
    $this->db->insert_batch('poll', $insert_data); 
}
public function poll_data($uid)
{
    $sql = "SELECT cat_id,vote,nomination_name,name FROM poll p JOIN nominations n ON p.vote=n.id JOIN category c ON c.id=cat_id WHERE user=".$uid;
		$result=$this->db->query($sql);
		//echo $this->db->last_query();exit;
		return $result->result();
}
public function if_polled($uid)
{
    	$sql = "SELECT count(id) as count FROM poll WHERE user=".$uid;
		$result=$this->db->query($sql);
		//echo $this->db->last_query();exit;
		return $result->result_array();
}
}
?>