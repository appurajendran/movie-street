<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
        {
				parent::__construct();
				
	$this->load->model('Register_model','RM');
	$this->load->model('Result_model','RSM');
                
        }

	public function index()
	{ 
	    $this->load->view('header');
	    $this->load->view('register');
	    $this->load->view('footer');
	}
	public function complete()
	{
	    $name=$this->input->post('name');
	    $email=$this->input->post('email');
	    $mobile=$this->input->post('mobile');
	    
	    $already=$this->RM->already($name,$email,$mobile);
	   // echo count($already);
	    if(count($already)==0)
	    {
	         $ticket=$this->insertdata($name,$email,$mobile);

	    $this->sendemail($name,$email,$mobile,$ticket);
	        
	    }
	   // else{
	   //     echo 'already exist';
	   // }
	    redirect(base_url().'register/success?email='.$email.'&mobile='.$mobile.'&name='.$name);
	   
	    
	    
	    
	}
	public function sendemail($name,$email,$mobile,$ticket)
	{
	    $name=ucwords($name);
require getcwd().'/application/libraries/vendor/autoload.php'; // If you're using Composer (recommended)

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\"personalizations\":[{\"to\":[{\"email\":\"$email\",\"name\":\"$name\"}],\"dynamic_template_data\":{\"verb\":\"\",\"adjective\":\"$ticket\",\"noun\":\"$name\",\"currentDayofWeek\":\"\"},\"subject\":\"Movie Street Film Award 2020\"}],\"from\":{\"email\":\"noreply@moviestreet.in\",\"name\":\"Moviestreet\"},\"reply_to\":{\"email\":\"admin@moviestreet.in\",\"name\":\"MS AWARDS ADMIN\"},\"template_id\":\"d-deda59b7df5547aeab998597bfb419e5\"}",
  CURLOPT_HTTPHEADER => array(
    "authorization: Bearer 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",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}



	}
	public function insertdata($name,$email,$mobile)
	{
	    $insert_data=array('name'=>$name,
	                        'email'=>$email,
	                        'mobile'=>$mobile);
	    $ticket_id= $this->RM->batch_insert($insert_data);
	    
	    return $ticket_id;
	    
	}
	public function success()
	{
	    $email=$this->input->get('email');
	    $name=$this->input->get('name');
	    $mobile=$this->input->get('mobile');
	    
	    $data=array('email'=>$email,
	                'name'=>$name,
	                'mobile'=>$mobile);
	       
	   $this->load->view('header');
	   $this->load->view('success',$data);
	   $this->load->view('footer');
	
	    
	}
	public function resendemail()
	{
	    $name=$this->input->get('name');
	    $email=$this->input->get('email');
	    $mobile=$this->input->get('mobile');
	    
	    $ticket=$this->RM->already($name,$email,$mobile);
	   // print_r($ticket[0]['id']);
	    $this->sendemail($name,$email,$mobile,$ticket[0]['id']);
	    redirect(base_url().'register/resend_success?&name='.$name.'&email='.$email);
	}
	public function resend_success()
	{
	   $this->load->view('header');
	   $this->load->view('resend_success');
	   $this->load->view('footer');
	}
	public function get_registered()
	{
	    if($this->session->userdata()['admin_session']['id']=='')
	    {
	        redirect(base_url().'login');
	    }
	    else{
	    $registered=$this->RSM->get_registered();
	   // print_r($registered);
	    $data=array('registered'=>$registered);
	    
	  //  $this->load->view('header');
	   $this->load->view('registered',$data);
	  // $this->load->view('footer');
	} 
	
}}
