<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Authentication extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        // Load facebook library
        $this->load->library('facebook');
        
        //Load user model
        $this->load->model('user');
    }
    public function index()
    {
        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }
    public function check_add_user()
    {
        $data=$this->input->post('response');
        $response=$this->user->checkUser($data);
        
        $this->session->set_userdata('user_session',$data);
        echo json_encode("Success from Controller");
    }
    public function logout() {
        
        $this->session->sess_destroy();
        redirect(base_url());
    }
}