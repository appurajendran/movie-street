<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
        {
				parent::__construct();
				
				$this->load->model("result_model","RM");
			
                
        }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
	    $this->load->view('header');
		$this->load->view('login_admin');
		$this->load->view('footer');
	}
	public function validate()
	{ 
	    $username=$this->input->post('username');
	    $password=$this->input->post('password');
	    
	    $validate=$this->RM->validate($username,$password);
	    
	    $this->session->set_userdata('admin_session', $validate[0]);
	    
	    redirect(base_url().'register/get_registered');
	    //print_r($validate);
	}
	
}
