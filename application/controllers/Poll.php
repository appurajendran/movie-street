<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poll extends CI_Controller {

	public function __construct()
        {
				parent::__construct();
				
		//	print_r($this->session->userdata());
				$this->load->model("poll_model","PM");
				$session_data=$this->session->userdata();
				// print_r($this->session->userdata('userData')); exit;
                // Your own constructor code
               if($session_data['user_session']['id']=="")redirect(base_url());
                
        }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
	    $session_data=$this->session->userdata();
	    $uid=$session_data['user_session']['id'];
	    $if_polled=$this->PM->if_polled($uid);
	  //  print_r($if_polled[0]['count']);
	  
	  if($if_polled[0]['count'] !=0) redirect(base_url().'poll/share');
	   
		$nominations=$this->PM->get_nominations();
		$data=array('nominations'=>$nominations);
		$this->load->view('header');
		$this->load->view('poll',$data);
		$this->load->view('footer');
	}
	public function insert_vote()
	{
	  
	    $poll_data=$_POST;
	    $uid=$this->session->userdata()['user_session']['id'];
	    
	    foreach ($poll_data as $name => $value) {
        
        //echo $name." ".$value."</br>";
        $insert_data[]=array('user'=>$uid,
                           'category'=>$name,
                           'vote'=>$value
                            );
   
    
}

$poll_insert=$this->PM->poll_insert($insert_data);

//print_r($insert_data);
	    redirect(base_url().'poll/share');
	    
	}
	public function share()
	{
	    $uid=$this->session->userdata()['user_session']['id'];
	    $poll_data=$this->PM->poll_data($uid);
	    $data=array('poll_data'=>$poll_data);
	    $this->load->view('header');
		$this->load->view('soon',$data);
		$this->load->view('footer');
	
	    
	}
}
