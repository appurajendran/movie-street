<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends CI_Controller {

	public function __construct()
        {
				parent::__construct();
				
		//	print_r($this->session->userdata());
				$this->load->model("result_model","RM");
			
                
        }

	public function index()
	{ 
	    $get_total_count=$this->RM->get_total_count();
	  
	    $all_nominations=$this->RM->all_nominations();
	    $get_vote=array();
	    //print_r($all_nominations);
	    $i=0;
	    foreach($all_nominations as $row)
	    {
	        $cat_id = $row->cat_id;
	        $id = $row->id;
	        $get_vote[$i]=$this->RM->get_vote($cat_id,$id);
	        
	        
	        $i=$i+1;
	      
	        
	    }
	   //print_r($get_vote);
	   //for($i=0;$i<=count($get_vote);$i++)
	   //{
	   //    print_r($get_vote[$i][0]);
	   //    echo '<br>';
	   //}
	   $data=array('result'=>$get_vote,
	                'get_total_count'=>$get_total_count);
	    $this->load->view('header');
		$this->load->view('result',$data);
		$this->load->view('footer');
	   
	    
	}
	
	
	public function login_admin(){
	    $this->load->view('header');
		$this->load->view('login_admin');
		$this->load->view('footer');
	}
		public function logout()
		{
		    
		    $this->session->sess_destroy();
		    redirect(base_url().'login');
	    
	}
}