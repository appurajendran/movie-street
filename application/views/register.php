<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!--
    <div class="wrapper">
       
       <div class="confirm-head">
           <h4>Join Us @Kochi</h4>
           <span></span>

           
 
       
      
           <form class="" method="post" action="<?php echo base_url()?>register/complete" style="width: 95%;">
          <div class="form-group">
          <label class="form-control-static">Name</label>
          <input type="text" class="form-control" name="name" autocomplete="off"  required/>
          </div>
          
          <div class="form-group">
          <label class="form-control-static">Mobile Number</label>
          <input type="text" class="form-control" name="mobile" autocomplete="off" required/>
          </div>
          
          <div class="form-group">
          <label class="form-control-static">Email ID</label>
          <input type="text" class="form-control" name="email" autocomplete="off" required/>
          </div>
         
          
          <div class="form-group">
         
          <input type="submit" class="form-control btn btn-success" />
          </div>
          
          </form>
     

<script>
   
</script>



    </div> 

</body>

</html>  -->


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Moviestreet Film Awards 2020</title>

    
    <!--Bootstrap-->
    <link rel="stylesheet" href="<?php echo base_url()?>register_assets/css/bootstrap.min.css" />
    <!-- Style -->
    <link rel="stylesheet" href="<?php echo base_url()?>register_assets/css/style.css" />
    <!--Font Family-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
</head>

<body>

    <section class="login-section">
        <div >
            <!--<img class="login-bk-img" src="<?php echo base_url()?>assets/banner.jpg">-->
        </div>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div class="card text-center"> 
                    <div class="card-header" style="width:200px;margin: 0 auto;">
                       <img src="<?php echo base_url()?>assets/logo.png" class="img-fluid img-responsive">
                    </div>
                    <div class="card-body">
                        <form class="" method="post" action="<?php echo base_url()?>register/complete">
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><img src="<?php echo base_url()?>register_assets/images/em.svg"></span>
                                </div>
                                
                                <input type="text" class="form-control" name="name" autocomplete="off" placeholder="Name" required/>
                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><img src="<?php echo base_url()?>register_assets/images/em.svg"></span>
                                </div>
                               <input type="email" class="form-control" name="email" placeholder="Email" autocomplete="off" required/>
                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><img src="<?php echo base_url()?>register_assets/images/em.svg"></span>
                                </div>
                               <input type="mobile" class="form-control" name="mobile" autocomplete="off" placeholder="Mobile" required/>
                            </div>
                       
                            <div class="form-group">
                                <button type="submit" value="Register Now" class="btn login_btn">REGISTER NOW</button>
                            </div>
                           
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
    </section>
  
    

</body>

</html>
