<div class="wrapper-home">
    
    <header class="wr-header">
            <div class="logo-container">
                <img class="logo-img" src="<?php echo base_url()?>assets/logo.png">
            </div>
    </header>
    <div class="home-content-area">
        <div class="banner-area">
            
            <img src="<?php echo base_url()?>assets/banner.jpg" class="img-responsive">
        </div>
        
        <div class="content-area-wrap">
            
            <h2>MOVIE STREET</h2>
            
            <p style="text-align: justify;">MOVIE STREET represents an audience among society having numerous views which differs from each other even on same subjects. We provide the platform to them for the expressions of those views, and we stand for converging all those towards each individual for their self realization of the unity in diversities and thereby for the development of a matured and sterling viewer society.

MOVIE STREET is all set to solemnize the 4th edition of MOVIE STREET FILM AWARDS this year. Welcome to the polling to pick out the winners of Movie Street Film Awards 2020 that will be held on 2nd February 2020 at Ernakulam Muncipal Town Hall.</p>
            
            <div class="l-with-fb">
                <?php 
                
//$authURL = preg_replace ('(redirect_uri.*)', 'redirect_uri=https://moviestreet.in/poll&scope=public_profile', $authURL);
                ?>
                <a class="login-btn" href="javascript:void(0);"  onclick="fbLogin()" id="fbLink">Login with facebook</a>
                
                <div id="userData"></div>
            </div>
            
            <p style="text-align: justify;">Out of the total 18 categories, 12 of them (with each of 15 nominations) are elected by considering the Poll Results by audience along with the decisions of Jury Panel. Members and Non members of Movie Street Group can participate in the polling.

One should poll for all categories and do submission. Note that no editing will be allowed after submitting.

After final submission, a tabled culmination of your choices will be displayed in the next window. Sharing this data to your personal timeline is feasible.
<br>
Happy Polling.</p>
        </div>
    </div>
    
</div>

  <div id="fb-root"></div>




<script>
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
      appId      : '2202609876445061', // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v5.0' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
            console.log(response);
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        
        //console.log(response); 
        
        $.ajax({
    url : "<?php echo base_url()?>User_Authentication/check_add_user",
     dataType:"JSON",
    data : {"response":response},
    async:"false",
    method:"POST",
    success: function(data, textStatus, jqXHR)
    {
    
  
       console.log("data Success");
       window.location.href = "/poll";
        //data - response from server
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
         console.log("data Fail");
        console.log(jqXHR);
    }
});
        
        
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function() {
        document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="fblogin.png"/>';
        document.getElementById('userData').innerHTML = '';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
    });
}
</script>