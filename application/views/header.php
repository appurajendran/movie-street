<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<html>


<head>

    <meta charset="utf-8">





    <!-- Page title -->
    <title>MOVIESTREET.IN</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

	<meta name="viewport" content="width=device-width, initial-scale=1">
    
    
<meta property="og:title" content="MOVIESTREET FILM AWARDS 2020" /> 
<meta property="og:image" content="http://moviestreet.in/assets/thump.jpg" />
<meta property="og:image:secure_url" content="https://moviestreet.in/assets/thump.jpg" /> 
<meta property="og:image:type" content="image/jpeg" /> 
<meta property="og:image:width" content="400" /> 
<meta property="og:image:height" content="300" />

<meta name="description" content="Moviestreet -Express Entertainment" />




    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"
        integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0=" crossorigin="anonymous" />
    <!--FONT-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,600i,700,800&display=swap"
        rel="stylesheet">


        
    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/dist/css/bootstrap.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/styles/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">



</head>
<body class="fixed-navbar fixed-sidebar">

<!-- Simple splash screen-->
<!-- <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>MovieStreet Awards</h1><p>2020 </p><div class="spinner"> <div class="rect1"></div> <div class="rect2"></div> <div class="rect3"></div> <div class="rect4"></div> <div class="rect5"></div> </div> </div> </div> -->
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Header -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-156255605-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-156255605-1');
</script>

