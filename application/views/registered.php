
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title -->
    <title>HOMER | WebApp admin theme</title>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

    <!-- Vendor styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fooTable/css/footable.core.min.css" />

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/styles/style.css">

</head>
<body class="fixed-navbar fixed-sidebar">


<!-- Header -->
<div id="header">
    <div class="color-line">
    </div>
    <div id="logo" class="light-version">
        <span>
            Moviestreet
        </span>
    </div>
    <nav role="navigation">
        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
        <div class="small-logo">
            <span class="text-primary">Moviestreet</span>
        </div>
       <a class="btn btn-warning pull-right" href="<?php echo base_url()?>result/logout">Logout</a>
       
    </nav>
</div>


<!-- Main Wrapper -->
<div id="wrapper">

    <div class="normalheader transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body">
                <a class="small-header-action" href="#">
                    <div class="clip-header">
                        <i class="fa fa-arrow-up"></i>
                    </div>
                </a>

            </div>
        </div>
    </div>


<div class="content animate-panel">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                
                
                 
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    Moviestreet Awards 2020
                </div>
               
                <div class="panel-body">
                    <input type="text" class="form-control input-sm m-b-md" id="filter" placeholder="Search..">
                    <table id="example1" class="footable table table-stripped toggle-arrow-tiny" data-page-size="8" data-filter=#filter>
                        <thead>
                        <tr>
                            <th data-toggle="true">Sl.No.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th data-hide="all">Phone</th>
                           
                            <th data-hide="all">Send Email</th>
                            <th data-hide="all">Timestamp</th>
                           
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($registered as $row){?>
                        <tr>
                            <td><?php echo $row->id;?></td>
                            <td><?php echo $row->name;?></td>
                            <td><?php echo $row->email;?></td>
                            <td><?php echo $row->mobile;?></td>
                           
                            <td><span class="pie">
                                <form target="_blank" class="" method="get" action="<?php echo base_url()?>register/resendemail">
    <input type="hidden" value="<?php echo $row->email?>" name="email" />
    <input type="hidden" value="<?php echo $row->name?>" name="name" />
    <input type="hidden" value="<?php echo $row->mobile?>" name="mobile" />
    
    <input style="margin-top: 20px;" class="btn btn-danger form-control" type="submit" value="Send Mail"/>
</form>  
                                
                            </span></td>
                            <td><?php echo $row->timestamp;?></td>
                           
                        </tr>
                        <?php } ?>
                   
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>

                </div>
            </div>

          
        </div>

    </div>
    </div>

    <!-- Footer-->
    <footer class="footer">
        <span class="pull-right">
            Moviestreet Awards
        </span>
        2020
    </footer>

</div>



<!-- Vendor scripts -->
<script src="<?php echo base_url()?>assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url()?>assets/vendor/sparkline/index.js"></script>
<script src="<?php echo base_url()?>assets/vendor/fooTable/dist/footable.all.min.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url()?>assets/scripts/homer.js"></script>


<script>

    $(function () {

        // Initialize Example 1
        $('#example1').footable();

        // Initialize Example 2
        $('#example2').footable();


    });

</script>

</body>

<!-- www/footable.html SCROLLSTECH DESIGN TEAM , Thu, 21 Apr 2016 11:03:28 GMT -->
</html>