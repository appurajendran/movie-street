// ============================================
// As of Chart.js v2.5.0
// http://www.chartjs.org/docs
// ============================================



var chart    = document.getElementById('chart').getContext('2d'),
    gradient = chart.createLinearGradient(0, 0, 0, 450);
    gradient1 = chart.createLinearGradient(0, 0, 0, 450);


gradient.addColorStop(0, 'rgba(58,210,153,0.5)');
gradient.addColorStop(0.5, 'rgba(58,210,153,0.05)');
gradient.addColorStop(1, 'rgba(58,210,153,0.05)');

gradient1.addColorStop(0, 'rgba(251,203,178,0.5)');
gradient1.addColorStop(0.5, 'rgba(251,203,178,0.05)');
gradient1.addColorStop(1, 'rgba(251,203,178,0.05)');





var data  = {
    labels: [ 'January', 'February', 'March', 'April', 'May', 'June' ],
    datasets: [
			{
			label: 'Custom Label Name',
			backgroundColor: gradient,
			pointBackgroundColor: 'white',
			borderWidth: 1,
			borderColor: '#20D999',
    data: [10, 3, 15, 35, 25, 35]
    },
				{
			label: 'Custom Label Name',
			backgroundColor: gradient1,
			pointBackgroundColor: 'white',
			borderWidth: 1,
			borderColor: '#FBB794',
			data: [0, 10, 25, 13, 35]
    }]
};


var options = {
  responsive: true,
  bezierCurve: false,
	maintainAspectRatio: true,
	animation: {
		easing: 'easeInOutQuad',
		duration: 520
	},
	scales: {
		xAxes: [{
      display: false,
			gridLines: {
				color: 'rgba(200, 200, 200, 0.05)',
				lineWidth: 1
			}
		}],
		yAxes: [{
			gridLines: {
				color: 'rgba(200, 200, 200, 0.08)',
				lineWidth: 1
			}
		}]
	},
	elements: {
		line: {
			tension: 0
		}
	},
	legend: {
		display: false
	},
	point: {
		backgroundColor: 'white'
	},
	tooltips: {
		titleFontFamily: 'Open Sans',
		backgroundColor: 'rgba(0,0,0,0.3)',
		titleFontColor: 'red',
		caretSize: 5,
		cornerRadius: 2,
		xPadding: 10,
		yPadding: 10
	}
};


var chartInstance = new Chart(chart, {
    type: 'line',
    data: data,
		options: options
});


/* Donut */
new Chart(document.getElementById("doughnut-chart"), {
  type: 'doughnut',
  data: {
    aspectRatio: 1,
    labels: [],
    datasets: [
      {
        label: "Population (millions)",
        backgroundColor: ["#53C4CE", "#0E62A3","#7CCDF2","#D8D7D7"],
        data: [35,30,20,29]
      }
    ]
  },
  options: {
    title: {
      display: true,
    },
    responsive: true,
    cutoutPercentage: 68,
    aspectRatio: 1  
  }
});





/* Vertical Bar */

 
bargradient = chart.createLinearGradient(0, 0, 0, 450);
bargradient1 = chart.createLinearGradient(0, 0, 0, 450);

bargradient.addColorStop(0, 'rgba(209,19,7,1)');   
bargradient.addColorStop(0.5, 'rgba(250,174,50,0.25)');
bargradient.addColorStop(0.5, 'rgba(250,174,50,0.25)');

bargradient1.addColorStop(0, 'rgba(7,79,247,1)');
bargradient1.addColorStop(0.5, 'rgba(55,193,206,0.25)');


new Chart(document.getElementById("bar-chart-horizontal"), {
  type: 'horizontalBar',
  data: {
    labels: ["Expenses", "Income"],
    datasets: [
      {
        label: "Population (millions)",
        backgroundColor: [bargradient, bargradient1],
        data: [2478,5267,734,784,433],
      }
    ]
  },
  options: {
    responsive: true,
    legend: { display: false },
    title: {
      display: true,
      text: 'Income and Expeness'
    },
    
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true,
                padding: 20,
                barThickness : 23
            }
        }]
    }
  }
});


$(document).ready(function(){

	$('#bar1').barfiller({ barColor: ' linear-gradient(to right, #FF6258 , #D11307)' });
	$('#bar2').barfiller({ barColor: ' linear-gradient(to right, #37C1CE , #0C51F2)' });
 
 
  // $("#nav-toggle").click(function () {
  //   $(".menu").toggle()
  //   });

    $('#nav-toggle').click(function() {
      $('.menu').toggleClass("close");
  });
});

